# MejorandroidCodigoBase

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/zekeApps/MejorandroidCodigoBase?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
Actualizado para usar con **AndroidStudio**

Este es el proyecto/ejemplo que Aurora trabajo en el curso de Android. 
**NO** esta terminado y esa es **la principal intencion**, que sigas el curso paso a paso haciendo tu mismo el codigo. 

Ademas de pasarla a Android Studio se hicieron algunas correciones en *AndroidManifest.xml* entre otros archivos que provocaban que dicho paquete no pudiera instalarse en el emulador y por consiguiente no ejecutaba.

El proyecto esta funcional justo donde se obtienen el objeto JSON de el hashtag **(minuto 39:30)**.
Sin embargo tienes que considerar lo siguiente antes de ejecutarlo

### 1 - ConstantsUtils.class
Yo hice mi propio ejemplo, no use los consumersKeys ni Consumer Secret ni el hashtag que uso ella...
Igual, esto en lugar de verlo como un problema esta intencionado para que practiques creando una App en Twitter
y los keys que te genera, solo pegalos en las respectivas variables

Esto aplica para:

- *CONSUMER_KEY*

- *CONSUMER_SECRET*

- Y el *hashtag deseado*



### 2 - build.gradle
Aurora usaba un *targetSdkVersion* menor al que use yo... por razones personales yo uso API v21 (Lollipop) contemplando KitKat (v19) como *minSdkVersion*
Si deseas incluir mas rango, solo es cuestion que modiques este archivo.

*Probado en AndroidStudio v1.0.3*
